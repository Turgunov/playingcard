//
//  CMMotionManager+shared.swift
//  PlayingCard
//
//  Created by Olimjon Turgunov on 11/29/17.
//  Copyright © 2017 Olimjon Turgunov. All rights reserved.
//

import CoreMotion

extension CMMotionManager {
    static var shared = CMMotionManager()
}
